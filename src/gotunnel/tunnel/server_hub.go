//
//   date  : 2014-06-06
//   author: xjdrew
//

package tunnel

import (
//	"encoding/json"
//	"math/rand"
	"net"
//	"os"
	"time"
)

type ServerHub struct {
	*Hub
}

func (self *ServerHub) addTunnel(tun *Tunnel) {
	self.tunnels = append(self.tunnels, tun)
}

func (self *ServerHub) handleLink(linkid uint16, link *Link) {
	defer self.Hub.ReleaseLink(linkid)
	defer Recover("serverHub handleLink")

	dest, err := net.Dial("tcp", conf.Upstream)
	if err != nil {
		Error("link(%d) connect to host failed, host:%s, err:%v", linkid, conf.Upstream, err)
		link.SendClose()
		return
	}

	Info("link(%d) new connection to %v", linkid, dest.RemoteAddr())

	conn := dest.(*net.TCPConn)
	conn.SetKeepAlive(true)
	conn.SetKeepAlivePeriod(time.Second * 60)
	link.Pump(conn)
}

func (self *ServerHub) Reload() error {
	return nil
}

func (self *ServerHub) Start() error {
	err := self.Reload()
	if err != nil {
		return err
	}

	//self.Hub.Start()
	return nil
}

func newServerHub() *ServerHub {
	serverHub := new(ServerHub)
	hub := newHub()
	serverHub.Hub = hub

	return serverHub
}
