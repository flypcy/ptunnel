//
//   date  : 2014-06-05
//   author: xjdrew
//

package tunnel

import (
//	"bytes"
//	"encoding/binary"
	"net"
	"io"
	"errors"
	"math/rand"
)


type Hub struct {
	*LinkSet
	tunnels []*Tunnel
}

func (self *Hub) createTunnel() (tun *Tunnel, err error) {
	conn, err := net.Dial("tcp", conf.Upstream)
	if err != nil {
		return
	}
	Info("create tunnel: %v <-> %v", conn.LocalAddr(), conn.RemoteAddr())

	// auth
	challenge := make([]byte, TaaBlockSize)
	if _, err = io.ReadFull(conn, challenge); err != nil {
		Error("read challenge failed(%v):%s", conn.RemoteAddr(), err)
		return
	}
	Debug("challenge(%v), len %d, %v", conn.RemoteAddr(), len(challenge), challenge)

	a := NewTaa(conf.Secret)
	token, ok := a.ExchangeCipherBlock(challenge)
	if !ok {
		err = errors.New("exchange chanllenge failed")
		Error("exchange challenge failed(%v)", conn.RemoteAddr())
		return
	}

	Debug("token(%v), len %d, %v", conn.RemoteAddr(), len(token), token)
	if _, err = conn.Write(token); err != nil {
		Error("write token failed(%v):%s", conn.RemoteAddr(), err)
		return
	}

	tun = newTunnel(conn.(*net.TCPConn), a.GetRc4key())
	return
}

func (self *Hub) getBestTunnel() *Tunnel {
	n := len(self.tunnels)

	tun := self.tunnels[rand.Intn(n)]

	for i := 0; i < n; i++ {
		tun1 := self.tunnels[i]
		if tun1.GetLoad() < tun.GetLoad() {
			tun = tun1
		}
	}

	return tun
}

func (self *Hub) Send(payload *TPayload) bool {
		
	tun := self.getBestTunnel()

	Info("write ===> %s link[%d],seq[%d],type[%d],size[%d]", tun.String(),payload.Linkid, payload.Seq,payload.Type,len(payload.Data))
	tun.Write2Chan(payload)

	return true
}

func (self *Hub) data(payload *TPayload) {
	
	linkid := payload.Linkid
	link := self.getLink(linkid)

	if link == nil {
		mpool.Put(payload.Data)
		Error("can not find link[%d]", linkid)
		return
	}

	if !link.putData(payload) {
		mpool.Put(payload.Data)
		Error("link[%d] put data failed", linkid)
		return
	}

	return
}

func (self *Hub) readTunnel(tun *Tunnel, oncreatelink func(uint16)) {
	defer tun.Close()

	for {
		payload, err := tun.Read()
		if err != nil {
			Error("%s read failed:%v", tun.String(), err)
			break
		}
				
		Info("read <=== %s link[%d],seq[%d],type[%d],size[%d]", tun.String(), payload.Linkid, payload.Seq,payload.Type,len(payload.Data))

		if payload.Type == LINK_CREATE {
			if oncreatelink == nil {
				Error("can not create createlink cmd")
				return
			}

			oncreatelink(payload.Linkid)

		} else {

			self.data(payload)
		}
	}
}

func (self *Hub) Status() {
	for i, tun := range self.tunnels {
		Log("<status> [%s] %s, queue[%d]", i, tun.String(), tun.GetLoad())
	}
}


func (self *Hub) NewLink(linkid uint16) *Link {
	link := &Link{
		id:    linkid,
		hub:   self,
		rbuf:  NewPBuf(linkid),
		sflag: true,
		Flow: 0,
		seq: 0}

	if self.setLink(linkid, link) {
		return link
	}

	return nil
}

func (self *Hub) ReleaseLink(linkid uint16) bool {
	return self.resetLink(linkid)
}

func newHub() *Hub {
	hub := new(Hub)
	hub.LinkSet = newLinkSet(uint16(conf.MaxLink))
	return hub
}
