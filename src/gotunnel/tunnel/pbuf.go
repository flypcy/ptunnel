//
//   date  : 2014-12-01
//   author: xjdrew
//

package tunnel

import "sync"

func (t *TPayload) Less(other interface{}) bool {
	return t.Seq < other.(*TPayload).Seq
}

type PBuf struct {
	cur    uint16
	linkid uint16
	pq     *PriorityQueue
	cond   *sync.Cond // buffer notify
	closed bool
}

func (b *PBuf) setCur(cur uint16) {
	b.cur = cur
}

func (b *PBuf) Close() bool {
	b.cond.L.Lock()
	defer b.cond.L.Unlock()

	if b.closed {
		return false
	}

	b.closed = true
	b.cond.Broadcast()
	return true
}

func (b *PBuf) Put(payload *TPayload) bool {
	b.cond.L.Lock()
	defer b.cond.L.Unlock()

	if b.closed {
		return false
	}

	b.pq.Push(payload)

	b.cond.Signal()
	return true
}

func (b *PBuf) Pop() (payload *TPayload, ok bool) {
	b.cond.L.Lock()
	defer b.cond.L.Unlock()

	if b.closed {
		ok = false
		return
	}

	for {
		if b.pq.Top() == nil {
			Info("link[%d] pbuf wait on empty **************", b.linkid)
			b.cond.Wait()
		} else if seq := b.pq.Top().(*TPayload).Seq; seq != b.cur {
			Info("link[%d] pbuf wait on: [%d], cur=[%d] ************", b.linkid, seq, b.cur)
			b.cond.Wait()
		} else {
			break
		}
	}

	payload = b.pq.Pop().(*TPayload)
	b.cur++
	ok = true
	return
}

func NewPBuf(lid uint16) *PBuf {
	var l sync.Mutex
	return &PBuf{
		cur:    0,
		pq:     NewPriorityQueue(),
		cond:   sync.NewCond(&l),
		closed: false,
		linkid: lid,
	}
}
