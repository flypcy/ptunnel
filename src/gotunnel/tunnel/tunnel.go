//
//   date  : 2014-06-04
//   author: xjdrew
//

package tunnel

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"net"
	"sync"
	"time"
	//"log"
	//"bytes"
	//"atomic"
	//"math/rand"
)

type TPayload struct {
	Linkid uint16
	Seq    uint16
	Type   uint8
	Data   []byte
}

type THeader struct {
	Linkid uint16
	Seq    uint16
	Type uint8
	Sz     uint16
}

type Tunnel struct {
	wlock  *sync.Mutex   // write lock
	writer *bufio.Writer // writer
	rlock  *sync.Mutex   // read lock
	reader *bufio.Reader // reader
	conn   *net.TCPConn  // low level conn
	desc   string        // description
	writechan chan *TPayload
}

func (t *Tunnel) Close() {
	t.conn.Close()
}

func NewPayload(typ uint8, linkid uint16, seq uint16, data []byte) *TPayload {
	return &TPayload{Linkid:linkid,Seq:seq,Type:typ,Data:data}
}

func (t *Tunnel) GetLoad() int {
	return len(t.writechan)
}

func (t *Tunnel) Write2Chan(payload *TPayload) {
	t.writechan <- payload
}

func (t *Tunnel) UpdateWrite() {
	for {
		payload := <- t.writechan 
		if err := t.Write(payload); err != nil {
			Panic("write tunnel[%s] failed!!!!!!", t.String())
		}
	}
}

func (t *Tunnel) Write(payload *TPayload) (err error) {
	//atomic.AddInt32(&t.load, 1)
	//defer atomic.AddInt32(&t.load, -1)

	defer mpool.Put(payload.Data)

	var header THeader
	header.Linkid = payload.Linkid
	header.Seq = payload.Seq
	header.Type = payload.Type
	header.Sz = uint16(len(payload.Data))

	t.wlock.Lock()
	defer t.wlock.Unlock()

	if err = binary.Write(t.writer, binary.LittleEndian, &header); err != nil {
		return
	}
	if _, err = t.writer.Write(payload.Data); err != nil {
		return
	}
	if err = t.writer.Flush(); err != nil {
		return
	}
	return
}

func (t *Tunnel) Read() (payload *TPayload, err error) {
	t.rlock.Lock()
	defer t.rlock.Unlock()

	var header THeader
	err = binary.Read(t.reader, binary.LittleEndian, &header)
	if err != nil {
		return
	}

	if header.Sz > conf.PacketSize {
		err = errors.New("malformed packet, too long")
		return
	}

	payload = &TPayload{}
	payload.Linkid = header.Linkid
	payload.Seq = header.Seq
	payload.Type = header.Type
	data := mpool.Get()[0:header.Sz]
	c := 0
	for c < int(header.Sz) {
		var n int
		n, err = t.reader.Read(data[c:])
		if err != nil {
			mpool.Put(data)
			return
		}
		c += n
	}
	payload.Data = data
	return
}

func (self *Tunnel) String() string {
	return fmt.Sprintf("%s", self.desc)
}

func newTunnel(conn *net.TCPConn, rc4key []byte) *Tunnel {
	desc := fmt.Sprintf("tunnel[%s <-> %s]", conn.LocalAddr(), conn.RemoteAddr())
	conn.SetKeepAlive(true)
	conn.SetKeepAlivePeriod(time.Second * 60)
	bufsize := int(conf.PacketSize) * 2
	
	Info("------------new Tunnel: %s", desc)
	
	return &Tunnel{
		wlock:  new(sync.Mutex),
		writer: bufio.NewWriterSize(NewRC4Writer(conn, rc4key), bufsize),
		rlock:  new(sync.Mutex),
		reader: bufio.NewReaderSize(NewRC4Reader(conn, rc4key), bufsize),
		conn:   conn,
		desc:   desc,
		writechan: make(chan *TPayload,10),
	}
}
