//
//   date  : 2014-06-05
//   author: xjdrew
//

package tunnel

import (
	//"bytes"
	//"encoding/binary"
	//	"net"
	//	"io"
	//	"errors"
	"time"
)

type ClientHub struct {
	*Hub
}

func (self *ClientHub) data(payload *TPayload) {
	linkid := payload.Linkid
	link := self.getLink(linkid)

	if link == nil {
		mpool.Put(payload.Data)
		Error("link[%d] no link", linkid)
		return
	}

	if !link.putData(payload) {
		//mpool.Put(payload.Data)
		Error("link[%d] put data failed", linkid)
		return
	}
	return
}

func (self *ClientHub) Status() {
	/*
	total := 0
	links := make([]uint16, 100)
	for i := uint16(0); i < self.capacity; i++ {
		if self.links[i] != nil {
			if total < cap(links) {
				links[total] = i
			}
			total += 1
		}
	}
	if total <= cap(links) {
		links = links[:total]
	}
	Log("<status> %s, %d links(%v)", self.tunnel.String(), total, links)
	*/
}


func (self *ClientHub) Start() error {
	done := make(chan error, len(self.tunnels))
	for i := 0; i < len(self.tunnels); i++ {
		go func(index int) {
			defer Recover("client start")

			first := true
			for {
				tun, err := self.createTunnel()
				if first {
					first = false
					done <- err
					if err != nil {
						Error("tunnel %d connect failed", index)
						break
					}
				} else if err != nil {
					Error("tunnel %d reconnect failed", index)
					time.Sleep(time.Second)
					continue
				}
				
				Error("tunnel %d connect succeed", index)
				self.tunnels[index] = tun
				//TODO 错误处理
				go tun.UpdateWrite()
				self.readTunnel(tun, nil)
				self.tunnels[index] = nil
				Error("tunnel %d disconnected", index)
			}
		}(i)
	}

	Info("client hub start...")
	
	for i := 0; i < len(self.tunnels); i++ {
		err := <-done
		if err != nil {
			Info("client start error: ", err)
			return err
		}
	}

	Info("client start end...")
	
	return nil
}

func newClientHub(tunnelNum int) *ClientHub {
	clienthub := new(ClientHub)
	clienthub.Hub = newHub()

	clienthub.Hub.tunnels = make([]*Tunnel, tunnelNum)

	return clienthub
}
