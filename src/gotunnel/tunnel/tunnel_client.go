//
//   date  : 2014-07-16
//   author: xjdrew
//

package tunnel

import (
	//	"errors"
	//	"io"
	"net"
	"sync"
	"time"
)

type PortInfo struct {
	Port int          //port number
	Ln   net.Listener //listen socket
	Flow int64        //traffic now
}

type Ports struct {
	sync.RWMutex
	m map[int]*PortInfo
}

type TunnelClient struct {
	//ln   net.Listener
	hub *ClientHub
	//off  int // current hub
	wg sync.WaitGroup

	ports Ports
}

func (self *TunnelClient) handleConn(hub *ClientHub, conn BiConn, port int) {
	defer conn.Close()
	defer Recover("tunnelClient handleConn")

	linkid := hub.AcquireId()
	if linkid == 0 {
		Error("alloc linkid failed, source: %v", conn.RemoteAddr())
		return
	}
	defer hub.ReleaseId(linkid)

	Info("link[%d] create link, source: %v", linkid, conn.RemoteAddr())
	link := hub.NewLink(linkid)
	defer hub.ReleaseLink(linkid)

	defer func(p int) {
		self.ports.Lock()
		defer self.ports.Unlock()

		self.ports.m[p].Flow += link.Flow
	}(port)

	link.SendCreate()
	link.Pump(conn)

}

func (self *TunnelClient) listen(port int) {
	defer self.wg.Done()

	var err error
	var ln net.Listener
	ln, err = net.ListenTCP("tcp4", &net.TCPAddr{Port: port})
	if err != nil {
		Panic("listen failed:%v", err)
	}

	self.ports.Lock()
	self.ports.m[port] = &PortInfo{Port: port, Ln: ln, Flow: 0}
	self.ports.Unlock()

	for {
		conn, err := ln.Accept()
		if err != nil {
			Log("acceept failed:%s", err.Error())
			if opErr, ok := err.(*net.OpError); ok {
				if !opErr.Temporary() {
					break
				}
			}
			continue
		}
		Info("new connection from %v", conn.RemoteAddr())

		tcpConn := conn.(*net.TCPConn)
		tcpConn.SetKeepAlive(true)
		tcpConn.SetKeepAlivePeriod(time.Second * 60)
		go self.handleConn(self.hub, tcpConn, port)
	}
}

func (self *TunnelClient) StartPort(port int) {
	self.wg.Add(1)
	go self.listen(port)
}

func (self *TunnelClient) Start() error {

	self.hub = newClientHub(conf.TunnelCount)

	self.hub.Start()

	self.StartPort(conf.Listen)

	return nil
}

func (self *TunnelClient) Reload() error {
	return nil
}

func (self *TunnelClient) Wait() {
	self.wg.Wait()
	Log("tunnel client quit")
}

func (self *TunnelClient) Status() {
	self.hub.Status()

	self.ports.RLock()
	defer self.ports.RUnlock()
	for _, v := range self.ports.m {
		Log("port: %d flow: %d", v.Port, v.Flow)
	}
}

func NewTunnelClient() *TunnelClient {
	/*
	count := 1
	if options.TunnelCount > 0 {
		count = options.TunnelCount
	}
	*/
	return &TunnelClient{
		ports: Ports{m: make(map[int]*PortInfo)}}
}
