package tunnel

import (
	"encoding/json"
	"os"
)

type Conf struct {
	IsClient    bool
	Listen      int
	Upstream    string
	LogLevel    int
	Secret      string
	MaxLink     int
	PacketSize  uint16
	TunnelCount int
}

func ReadConf(conf_file string) *Conf {
	fp, err := os.Open(conf_file)
	if err != nil {
		Panic("open config file failed:%s", err.Error())
	}
	defer fp.Close()

	cf := new(Conf)
	dec := json.NewDecoder(fp)
	err = dec.Decode(cf)
	if err != nil {
		Panic("decode config file failed:%s", err.Error())
	}

	return cf
}
