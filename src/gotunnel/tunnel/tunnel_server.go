//
//   date  : 2014-06-05
//   author: xjdrew
//

package tunnel

import (
	"io"
	"net"
	"sync"
)

type TunnelServer struct {
	*ServerHub
	ln   net.Listener
	wg   sync.WaitGroup
	rw   sync.RWMutex
}

func (self *TunnelServer) handleConn(conn *net.TCPConn) {
	defer self.wg.Done()
	defer conn.Close()
	defer Recover("TunnelServer handleConn")

	Info("create tunnel: %v <-> %v", conn.LocalAddr(), conn.RemoteAddr())

	// authenticate connection
	a := NewTaa(conf.Secret)
	a.GenToken()

	challenge := a.GenCipherBlock(nil)
	Debug("challenge(%v), len %d, %v", conn.RemoteAddr(), len(challenge), challenge)
	if _, err := conn.Write(challenge); err != nil {
		Error("write challenge failed(%v):%s", conn.RemoteAddr(), err)
		return
	}

	token := make([]byte, TaaBlockSize)
	if _, err := io.ReadFull(conn, token); err != nil {
		Error("read token failed(%v):%s", conn.RemoteAddr(), err)
		return
	}

	Debug("token(%v), len %d, %v", conn.RemoteAddr(), len(token), token)
	if !a.VerifyCipherBlock(token) {
		Error("verify token failed(%v)", conn.RemoteAddr())
		return
	}

	tun := newTunnel(conn, a.GetRc4key())
	self.addTunnel(tun)
		
	Info("add new tunnel: %s", tun.String())
	
	//defer delete(self.tunnels, tun)
	// TODO delete tunnel
	
	//update write
	go tun.UpdateWrite()

	//TODO 错误处理

	//update read
	self.readTunnel(tun, func(linkid uint16) {

		link := self.NewLink(linkid)
		link.setRBufCur(1) //收到create消息Seq=0，故link当前接受seq=1

		Info("tunnel receive create link cmd link[%d], replay ack", linkid)
		
		link.SendCreateACK()

		go self.handleLink(linkid, link)
	})
}

func (self *TunnelServer) listen() {
	defer self.wg.Done()

	var err error
	self.ln, err = net.ListenTCP("tcp4", &net.TCPAddr{Port: conf.Listen})
	if err != nil {
		Panic("listen failed:%v", err)
	}

	for {
		conn, err := self.ln.Accept()
		if err != nil {
			Error("back server acceept failed:%s", err.Error())
			if opErr, ok := err.(*net.OpError); ok {
				if !opErr.Temporary() {
					break
				}
			}
			continue
		}
		Debug("back server, new connection from %v", conn.RemoteAddr())
		self.wg.Add(1)
		tcpConn := conn.(*net.TCPConn)
		go self.handleConn(tcpConn)
	}
}

func (self *TunnelServer) Start() error {
	if err := self.Reload(); err != nil {
		Panic("server reload error", err)
	}
	
	self.wg.Add(1)
	go self.listen()
	return nil
}

func (self *TunnelServer) Reload() error {
	self.rw.RLock()
	defer self.rw.RUnlock()
	
	self.ServerHub.Reload()
	
	Info("reload tunnel server call")
// TODO reload
	return nil
}

func (self *TunnelServer) Wait() {
	self.wg.Wait()
	Error("back hub quit")
}

func (self *TunnelServer) Status() {
	self.rw.RLock()
	defer self.rw.RUnlock()

	//TODO status
	self.Hub.Status()
}

func NewTunnelServer() *TunnelServer {
	return &TunnelServer{
		ServerHub: newServerHub()}
}
