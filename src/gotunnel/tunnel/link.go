//
//   date  : 2014-06-04
//   author: xjdrew
//

package tunnel

import (
	"bufio"
	"errors"
	"sync"
)


const (
	LINK_DATA uint8 = iota
	LINK_CREATE
	LINK_CREATE_ACK
	LINK_CLOSE
	LINK_CLOSE_RECV
	LINK_CLOSE_SEND
)

var errPeerClosed = errors.New("errPeerClosed")

type Link struct {
	id    uint16
	conn  BiConn
	hub   *Hub
	rbuf  *PBuf // 接收缓存
	sflag bool        // 对端是否可以收数据
	wg    sync.WaitGroup
	Flow  int64 //流量
	seq uint16 //当前seq
}

func (self *Link) setRBufCur(cur uint16) {
	self.rbuf.setCur(cur)
}

// stop write data to remote
func (self *Link) resetSflag() bool {
	if self.sflag {
		self.sflag = false
		// close read
		if self.conn != nil {
			self.conn.CloseRead()
		}
		return true
	}
	return false
}

// stop recv data from remote
func (self *Link) resetRflag() bool {
	return self.rbuf.Close()
}

// peer link closed
func (self *Link) resetRSflag() bool {
	ok1 := self.resetSflag()
	ok2 := self.resetRflag()
	return ok1 || ok2
}

func (self *Link) SendCreate() {
	self.Send(LINK_CREATE, nil)
	self.rbuf.Pop()
}

func (self *Link) SendCreateACK() {
	self.Send(LINK_CREATE_ACK, nil)
}

func (self *Link) SendClose() {
	if self.resetRSflag() {
		self.Send(LINK_CLOSE, nil)
	}
}

func (self *Link) putData(payload *TPayload) bool {
	return self.rbuf.Put(payload)
}

func (self *Link) handleError(readFailed bool) {

	if readFailed {
		if self.resetSflag() {
			self.Send(LINK_CLOSE_SEND, nil)
		}
	} else {
		if self.resetRflag() {
			self.Send(LINK_CLOSE_RECV, nil)
		}
	}
}

func (self *Link) Send(cmd uint8, data []byte) bool {
	payload := NewPayload(cmd, self.id, self.seq, data)
	if self.hub.Send(payload) {
		self.seq++
		return true
	}
	return false
}


func (self *Link) Ctrl(cmd uint8) {
	switch cmd {
		case LINK_CLOSE:
			self.resetRSflag()

		case LINK_CLOSE_RECV:
			self.resetSflag()

		case LINK_CLOSE_SEND:
			self.resetRflag()

		default:
			Error("link[%d] receive unknown cmd:%d", self.id, cmd)
	}

}

// read from link
func (self *Link) pumpIn() {
	defer self.wg.Done()
	defer self.conn.CloseRead()

	bufsize := int(conf.PacketSize) * 2
	rd := bufio.NewReaderSize(self.conn, bufsize)
	for {
		buffer := mpool.Get()
		n, err := rd.Read(buffer)
		if err != nil {
			Debug("link[%d] read failed:%v", self.id, err)
			self.handleError(true)
			mpool.Put(buffer)
			break
		}
		
		//Trace("link(%d) read %d", self.id, n)

		if !self.sflag {
			// receive LINK_CLOSE_WRITE
			mpool.Put(buffer)
			break
		}
		
		if !self.Send(LINK_DATA, buffer[:n]) {
			break
		}
	}
}

// write to link
func (self *Link) pumpOut() {
	defer self.wg.Done()
	defer self.conn.CloseWrite()

	for {
		payload, ok := self.rbuf.Pop()
		if !ok {
			break
		}

		if payload.Type == LINK_DATA {
			
			data := payload.Data
			
			_, err := self.conn.Write(data)
			mpool.Put(data)

			if err != nil {
				self.handleError(false)
				Debug("link[%d] write failed:%v", self.id, err)
				break
			}
			
			Trace("link[%d] write %d", self.id, len(data))
			
			self.Flow += int64(len(data))
			
		} else {
			self.Ctrl(payload.Type)
		}

		
	}
}

func (self *Link) Pump(conn BiConn) {
	self.conn = conn

	self.wg.Add(1)
	go self.pumpIn()

	self.wg.Add(1)
	go self.pumpOut()

	self.wg.Wait()
	Info("link[%d] closed", self.id)
}

