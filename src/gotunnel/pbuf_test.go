package main

import "testing"
import "gotunnel/tunnel"
import "time"
import "fmt"
import "math/rand"

func TestBuffer(t *testing.T) {

	for n := 0; n < 100; n++ {
		fmt.Println(rand.Intn(10))
	}

	fmt.Println("begin test pbuf")

	buf := tunnel.NewPBuf()

	go func() {
		time.Sleep(time.Second * 2)

		fmt.Println("begin put 1")

		buf.Put(&tunnel.TPayload{3, 3, 1,nil})

		fmt.Println("begin put 2")

		buf.Put(&tunnel.TPayload{1, 1, 1,nil})

		buf.Put(&tunnel.TPayload{2, 2, 1,nil})

		buf.Put(&tunnel.TPayload{0, 0,1, nil})

		buf.Put(&tunnel.TPayload{4, 4,1, nil})

		fmt.Println("begin put 3")
	}()

	//time.Sleep(time.Second * 3)

	var i uint16
	for i = 0; i < 5; i++ {
		fmt.Println("begin pop...")

		a, ok := buf.Pop()

		fmt.Println(i, a, ok)

		if !ok || a.Linkid != i {
			t.FailNow()
		}
	}

}
