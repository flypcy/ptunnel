//
//   date  : 2014-06-04
//   author: xjdrew
//

package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gotunnel/tunnel"
)

const SIG_RELOAD = syscall.SIGUSR1
const SIG_STATUS = syscall.SIGUSR2

func handleSignal(app *tunnel.App) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, SIG_RELOAD, SIG_STATUS, syscall.SIGTERM, syscall.SIGHUP)

	for sig := range c {
		switch sig {
		case SIG_RELOAD:
			tunnel.Log("catch signal reload")
			app.Reload()
		case SIG_STATUS:
			tunnel.Log("catch signal status")
			app.Status()
		default:
			tunnel.Log("catch signal:%v, ignore", sig)
		}
	}
}


func main() {
	
	cf := tunnel.ReadConf("conf.json")

	app := tunnel.NewApp(cf)
	
	if cf.IsClient {
		app.Add(tunnel.NewTunnelClient())
	} else {
		app.Add(tunnel.NewTunnelServer())
	}

	err := app.Start()
	if err != nil {
		fmt.Printf("start gotunnel failed:%s\n", err.Error())
		return
	}
	go handleSignal(app)

	app.Wait()
}
