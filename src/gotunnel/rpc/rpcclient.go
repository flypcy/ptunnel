package rpc

import (
	"thrift"
	"gen/webservice"
	"log"
)

type RpcClient struct {
	remoteAddr string
	transport thrift.TTransport
	Ws webservice.WebService
}

func NewRpcClient(addr string) *RpcClient {
	return &RpcClient{remoteAddr: addr}
}

func (this *RpcClient) Open() error {
	var err error
	var transport thrift.TTransport
	
	transportFactory := thrift.NewTTransportFactory()
	 
	transport, err = thrift.NewTSocket(this.remoteAddr)
	
	
	if err != nil {
		log.Println("open socket error: ", err)
		return err
	}

	transport = transportFactory.GetTransport(transport)
	
	if err = transport.Open(); err != nil {
		log.Println("open transport error: ", err)
		return err
	}
	
	protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()
	
	this.transport = transport
	this.Ws = webservice.NewWebServiceClientFactory(transport, protocolFactory)
	
	return nil
}

func (this *RpcClient) Close() {
	if this.transport != nil {
		this.transport.Close()
	}
}