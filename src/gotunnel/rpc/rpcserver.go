package rpc

import (
	"thrift"
	"gen/tunnelservice"
	"log"
)

type RpcServer struct {
	remoteAddr string
}

func NewRpcServer(addr string) *RpcServer {
	return &RpcServer{remoteAddr: addr}
}

func (this *RpcServer) Serve() error {
	var err error
	var transport thrift.TServerTransport
	
	transportFactory := thrift.NewTTransportFactory()
	 
	transport, err = thrift.NewTServerSocket(this.remoteAddr)
	
	
	if err != nil {
		log.Println("open socket error: ", err)
		return err
	}
	
	protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()
	
	handler := NewRpcHandler()
	
	processor := tunnelservice.NewTunnelServiceProcessor(handler)
	server := thrift.NewTSimpleServer4(processor, transport, transportFactory, protocolFactory)
	
	log.Println("start server ... on", this.remoteAddr)
	
	return server.Serve()
}
