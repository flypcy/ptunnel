package rpc

import (
	"testing"
	"git.apache.org/thrift.git/lib/go/thrift"
	"rpc/gen/tunnelservice"
)

func TestRpcServer(t *testing.T) {
	//t.Error("test rcp server error")
	addr := "127.0.0.1:7777"

	server := NewRpcServer(addr)
	go server.Serve()


	var err error
	var transport thrift.TTransport
	
	transportFactory := thrift.NewTTransportFactory()
	 
	transport, err = thrift.NewTSocket(addr)
	
	
	if err != nil {
		t.Error(err)
	}

	transport = transportFactory.GetTransport(transport)
	
	if err = transport.Open(); err != nil {
		t.Error(err)
	}
	
	protocolFactory := thrift.NewTBinaryProtocolFactoryDefault()
	
	client := tunnelservice.NewTunnelServiceClientFactory(transport, protocolFactory)

	if _, err = client.StartTunnel(0,0); err != nil {
		t.Error(err)
	}

	if _, err = client.StopTunnel(0); err != nil {
		t.Error(err)
	}

	if _, err = client.ResetTrafficLimit(0,0); err != nil {
		t.Error(err)
	}
}