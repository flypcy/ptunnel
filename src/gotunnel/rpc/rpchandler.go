package rpc

import (
	"log"
)

type RpcHandler struct {
	
}

func NewRpcHandler() *RpcHandler {
	return &RpcHandler{}
}

func (this *RpcHandler) StartTunnel(port int32, trafficLimit int64) (r int32, err error) {
	log.Println("receive start tunnel: ", port, trafficLimit)
	if port == 0 {
		return 0, nil
	}
	
	return 0, nil
}

func (this *RpcHandler) StopTunnel(port int32) (r int32, err error) {
	log.Println("receive StopTunnel: ", port)
	if port == 0 {
		return 0, nil
	}

	return 0, nil
}

func (this *RpcHandler) ResetTrafficLimit(port int32, trafficLimit int64) (r int32, err error) {
	log.Println("receive ResetTrafficLimit: ", port, trafficLimit)
	if port == 0 {
		return 0, nil
	}

	return 0, nil
}
