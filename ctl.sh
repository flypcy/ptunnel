#!/bin/bash


usage()
{
    echo "Usage: ${0##*/} {start|stop|status|restart}"
    exit 1
}

[ $# -gt 0 ] || usage


running()
{
    local PID=$(cat "$1" 2>/dev/null) || return 1
    kill -0 "$PID" 2>/dev/null
}

#####################################################
# Find a location for the pid file
#####################################################

if [ ! -d "./run" ]
    then
    mkdir "./run"
fi

RUN_PID="./run/tunnel.pid"

RUN_CMD=(bin/gotunnel)
LOG_FILE=tunnel.log

case "$1" in
    start)
        echo -n "Starting: "
        if [ -f "$RUN_PID" ]
            then
            if running $RUN_PID
                then
                echo "Already Running!"
                exit 1
            else
                # dead pid file - remove
                rm -f "$RUN_PID"
            fi
        fi

        nohup "${RUN_CMD[@]}" 2>&1 > "$LOG_FILE" &
#disown $!
        echo $! > "$RUN_PID"

#echo "STARTED tunnel `date`"

        if running "$RUN_PID"
        then
            echo "STARTED tunnel `date`"
        else
            echo "start failed!!!"
        fi


    ;;

    stop)
        echo -n "Stopping: "

        PID=$(cat "$RUN_PID" 2>"$LOG_FILE")
        kill -9 "$PID" 2>"$LOG_FILE"

        TIMEOUT=30
        while running $RUN_PID; do
            if (( TIMEOUT-- == 0 )); then
                kill -KILL "$PID" 2>"$LOG_FILE"
            fi

            sleep 1
        done

        rm -f "$RUN_PID"
        echo OK

    ;;

    restart)
        CTL_SH=$0

        "$CTL_SH" stop "$@"
        "$CTL_SH" start "$@"

    ;;

    status)
        if [ -f "$RUN_PID" ]
            then
            if running "$RUN_PID"
                then
                echo "Tunnel is Running now..."
            else
                echo "Tunnel is Stopped"
            fi
        else
            echo "Tunnel is Stopped"
        fi

    ;;


    *)
        usage

    ;;

esac

exit 0
